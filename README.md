# PogoShadowWarnIssue

## Description
This Tango class was created to show how we can trigger shadow warnings when compiling with `-Wshadow` option.
Some warnings can be triggered from Pogo generated code if the device server programmer creates a Tango Class with a class or device property named 'type' and has some attributes.

Having one attribute named 'type' can also trigger some of these `-Wshadow` warnings in the `PogoShadowWarnIssueClass::attribute_factory` method.
